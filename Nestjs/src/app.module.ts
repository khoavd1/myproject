import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RoleController } from './role/role.controller';
import { RoleModule } from './role/role.module';

@Module({
  imports: [RoleModule],
  controllers: [AppController, RoleController],
  providers: [AppService],
})
export class AppModule {}
