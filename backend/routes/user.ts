import express, { Request, Response, NextFunction } from 'express';
import User from '../models/User';
import Role from '../models/Role';
import { CheckValidate } from "../requests/users/CheckValidate";
const router = express.Router();

import { validate, validateMongoId } from "../validation";
import HttpStatus from "http-status-codes";
import { userInfo } from 'os';
import { ObjectId } from 'mongodb';


//API GET List User
router.get("/", async (req: Request, res: Response) => {
    const data = await User.find().populate("role");
    console.log(data);
     
    res.json(data);
});

//API ADD
router.post("/create", validate(CheckValidate), async (req: Request, res: Response) => {
    const data = new User(req.body);

    req.body['role'] = new ObjectId(req.body.role)

    const newUser = await data.save().then((doc) => doc.populate("role").execPopulate());
    res.json({
        message: "Thêm User thành công!",
        data: newUser,
    });
    // await data.save().then(() => res.json('Add Success!'))
    //     .catch((err: string) => res.status(400).json('Error: ' + err));
});


//API GET Detail
router.get('/:id', validateMongoId(), async (req: Request, res: Response) => {
    const data = await User.findById(req.params.id).populate("role");

    if (!User)
        return res.status(HttpStatus.NO_CONTENT).end();

    res.json(data);
});


// Edit user
router.put('/update/:id', validateMongoId(), validate(CheckValidate, {
    skipMissingProperties: true,
}), async (req: Request, res: Response) => {
    const { id } = req.params;
    if (!Object.keys(req.body).length) {
        return res.status(HttpStatus.NO_CONTENT).end();
    }
    const updateUser = await User.findOneAndUpdate({ _id: id }, req.body, {
        new: true,
    });

    if (!updateUser) {
        return res.status(HttpStatus.NO_CONTENT).end();
    }

    res.json({
        message: "Update thành công!",
        data: updateUser,
    });

    // await User.findByIdAndUpdate(id, {
    //     name: req.body.name,
    //     email: req.body.email,
    //     birthday: req.body.birthday,
    //     tel: req.body.tel,
    //     role: req.body.role
    // }, { upsert: true }, (e, raw) => {
    //     if (e) {
    //         res.status(400).send('Invalid user supplied');
    //     }
    //     res.send(raw);
    // });
});

//Delete User
router.delete('/:id', validateMongoId(),
    async (req: Request, res: Response) => {
        const { id } = req.params;
        await User.deleteOne({ _id: id }, (err) => {
            if (err)
                res.status(HttpStatus.NO_CONTENT);

        });
    });

export default router;