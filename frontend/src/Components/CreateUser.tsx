import React, { Component, Props, useReducer } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

// interface CreateUserProps {
// }

interface Role {
    _id: string;
    name: string;
}

interface CreateUserState {
    userData: {
        name?: string;
        birthday?: Date;
        email?: string;
        tel?: string;
        role?: string;
    };
    roles: Role[];
}

class CreateUser extends Component<CreateUserState> {
    state = {
        userData: {
            name: "", email: "", birthday: new Date(), tel: "", role: ""
        }, roles: []
    }
    componentDidMount() {
        axios.get("/api/roles").then((res) => {
            if (res.data.length > 0) {
                this.setState({
                    roles: res.data
                });
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    onChangeUser = (x: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                name: x.target.value
            }
        });
    };
    onChangeEmail = (x: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                email: x.target.value
            }
        });
    }
    onChangeDate = (date: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                birthday: date
            }
        })
    };

    onChangeTel = (x: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                tel: x.target.value
            }
        });
    };
    onChangeRole = (x: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                role: x.target.value
            }
        });
    };
    onSubmit = (x: any) => {
        x.preventDefault();
        const { name, email, birthday, tel, role } = this.state.userData
        const user = {
            name: name,
            email: email,
            birthday: birthday,
            tel: tel,
            role: role,
        };
        console.log(user);
        axios.post("/api/users/create", user).then((res) => {
            alert(res.data.message);
        });
        window.location.href = "/";
    };
    render() {
        return (
            <div>
                <h3>Tạo mới User</h3>
                <form action="" onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Tên:</label>
                        <input
                            name=""
                            id=""
                            ref="userInput"
                            required
                            className="form-control"
                            value={this.state.userData.name}
                            onChange={this.onChangeUser}
                            placeholder="Enter name..."
                            type="text"
                        />
                    </div>
                    <div className="form-group">
                        <label>Ngày Sinh:</label>
                        <div>
                            <DatePicker
                                selected={this.state.userData.birthday}
                                onChange={this.onChangeDate}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Số điện thoại:</label>
                        <input
                            type="text"
                            required
                            className="form-control"
                            placeholder="Enter telephone..."
                            value={this.state.userData.tel}
                            onChange={this.onChangeTel}
                        />
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input
                            type="text"
                            required
                            className="form-control"
                            placeholder="Enter email..."
                            value={this.state.userData.email}
                            onChange={this.onChangeEmail}
                        />
                    </div>
                    <div className="form-group">
                        <label>Quyền:</label>
                        <select
                            name="roleSelect"
                            required
                            className="form-control"
                            onChange={this.onChangeRole}>
                            {
                                this.state.roles.length ? this.state.roles.map((role: any, _) => (
                                    <option key={role._id} value={role._id}>{role.name}</option>
                                )) : null
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <input
                            type="submit"
                            value="Tạo"
                            className="btn btn-primary"
                        />
                    </div>
                </form>

            </div>
        );
    }
}

export default CreateUser;


