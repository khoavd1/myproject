import { Injectable, Inject } from '@nestjs/common';
import { BaseService } from 'src/Base/base.service';
import { RolesModel } from 'src/model/role';
import { convertId } from 'src/common/function';
import { Collection } from 'mongodb'

@Injectable()
export class RoleService extends BaseService<RolesModel> {
    constructor(@Inject('roles') collection: Collection<RolesModel>) {
        super(collection)
    }

    async getRoles(): Promise<RolesModel[]> {
        return this.find({})
    }

    async createRole(input: RolesModel): Promise<RolesModel> {
        const result = await this.insertOne(input)
        return result.insertedCount === 1 ? convertId(result?.ops[0]) : null
    }
}
