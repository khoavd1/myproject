import React, { Component } from 'react';

class TopMenu extends Component {
    render() {
        return (
            <div className="col-12">
                <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                    {/* Brand/logo */}
                    <a className="navbar-brand" href="#">MINORI</a>
                    {/* Links */}
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" href="/">List User</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/create">Create New</a>
                        </li>
                        {/* <li className="nav-item">
                            <a className="nav-link" href="#"></a>
                        </li> */}
                    </ul>
                </nav>
                {/* <div className="container-fluid">
                    <h3>Brand / Logo</h3>
                    <p>The .navbar-brand class is used to highlight the brand/logo/project name of your page.</p>
                </div> */}
            </div>
        );
    }
}

export default TopMenu;