import { BaseModel } from 'src/model/base'
import { ObjectID } from 'mongodb'

export function convertId<T extends BaseModel>(data: T): T {
    data.id = data._id.toHexString()
    data._id = undefined
    return data
}

export function buildFilterById<T extends BaseModel>(id: string): T {
    return { _id: new ObjectID(id) } as T
}


