//import { mongoDbProviders } from "../mongo-db/mongo-db.providers";
import { Module, DynamicModule, Provider } from "@nestjs/common";
import { Db } from 'mongodb';
import { MONGODB_PROVIDER } from 'src/constants';
import { mongoDbProviders } from "./mongo-db.providers";

@Module({})

export class MongoDbModule {
    static getCollectionService(collectionName: string): DynamicModule {

        return {
            module: MongoDbModule,
            providers: [...mongoDbProviders],
            exports: [...mongoDbProviders]
        }
    }
}