import {
    registerDecorator,
    ValidationOptions,
    ValidationArguments,
} from "class-validator";

import User from "../../models/User";

function IsUserAlreadyExits(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "isEmailAlreadyExist",
            async: true,
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return new Promise((ok) => {
                        User.find({ email: value }, (err, users) => {
                            users.length > 0 ? ok(false) : ok(true);
                        });
                    });
                },
            },
        });
    };
}

export default IsUserAlreadyExits;