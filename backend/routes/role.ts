import express, { Request, Response } from "express";
const router = express.Router();

import Role from "../models/Role";

router.get("/", async (req: Request, res: Response) => {
    const roles = await Role.find();

    res.json(roles);

});

export default router; 