import { UserModel } from 'src/model/user';
import { Collection } from 'mongodb';
import { Injectable, Inject } from '@nestjs/common';
import {
    ValidatorConstraint, ValidatorConstraintInterface
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: true })
@Injectable()
export class CheckEmailExits implements ValidatorConstraintInterface {
    constructor(@Inject('users') private collection: Collection<UserModel>) { }
    async validate(email: string): Promise<boolean> {
        const data = await this.collection.findOne({ email: email })
        return data ? false : true
    }

    defaultMessage(): string {
        return 'Email đã tồn tại'
    }
}