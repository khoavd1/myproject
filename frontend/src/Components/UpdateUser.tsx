import React, { Component, SyntheticEvent } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { RouteComponentProps } from 'react-router-dom';

type IdParams = {
    id: string;
};

interface Role {
    _id: string;
    name: string;
}

interface User {
    _id: string;
    name: string;
    birthday: Date;
    tel: string;
    email: string;
    role: Role
}

interface MatchParams extends RouteComponentProps<IdParams> { }
interface UpdateUserProps extends MatchParams { }


interface UpdateUserState {
    userData: User;
    roles: Role[];
    updateData: {};
}

class UpdateUser extends Component<UpdateUserProps, UpdateUserState> {
    constructor(props: UpdateUserProps) {
        super(props);
        this.state = {
            userData: {
                name: "", email: "", birthday: new Date(), tel: "", _id: "", role: {
                    _id: '',
                    name: ''
                }
            }, roles: [],
            updateData: {}
        };
    }
    componentDidMount() {
        axios.get("/api/roles/").then((res) => {
            if (res.data.length > 0) {
                this.setState({
                    roles: res.data,
                });
            }
        }).catch((error) => {
            console.log(error);
        });

        axios.get("/api/users/" + this.props.match.params.id).then(({ data }) => {
            this.setState({
                userData: {
                    ...data,
                    birthday: new Date(data.birthday)
                }
            });
        }).catch((error) => {
            console.log(error);
        });
    }


    onChangeUser = (e: any) => {
        if (e.target.value !== this.state.userData.name) {
            console.log('not equal')
            this.setState({
                updateData: {
                    ...this.state.updateData,
                    name: e.target.value
                }
            });
        } else {
            console.log('equal')
            this.setState({
                updateData: {
                    ...this.state.updateData,
                    name: undefined
                }
            })
        }
    };

    onChangeDate = (date: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                birthday: date.target.value
            }
        });
    };

    onChangeTel = (e: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                tel: e.target.value
            }
        });
    };

    onChangeEmail = (e: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                email: e.target.value
            }
        });
    };

    onChangeRole = (e: any) => {
        this.setState({
            userData: {
                ...this.state.userData,
                role: e.target.value
            }
        });
    };

    onSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        const { name, email, birthday, tel, role } = this.state.userData;
        const userUpdate = {
            name: name,
            birthday: birthday,
            tel: tel,
            email: email,
            role: role,
        };
        console.log(userUpdate);
        axios.put('/api/users/update/' + this.props.match.params.id, userUpdate).then((res) => console.log(res.data));
        //window.location.href = "/";
    };
    render() {
        return (
            <div>
                <h3>Edit User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name:</label>
                        <input
                            type="text"
                            name=""
                            id=""
                            className="form-control"
                            value={this.state.userData.name}
                            onChange={this.onChangeUser} />
                    </div>

                    <div className="form-group">
                        <label>Birthday:</label>
                        <div>
                            <DatePicker
                                selected={this.state.userData.birthday}
                                onChange={this.onChangeDate} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label>Telephone:</label>
                        <input
                            type="text"
                            onChange={this.onChangeTel}
                            className="form-control"
                            value={this.state.userData.tel} />
                    </div>

                    <div className="form-group">
                        <label>Email:</label>
                        <input
                            type="text"
                            onChange={this.onChangeEmail}
                            className="form-control"
                            value={this.state.userData.email} />
                    </div>

                    <div className="form-group">
                        <label>Role:</label>
                        <select
                            name="roleSelect"
                            id=""
                            onChange={this.onChangeRole}
                            className="form-control">

                            {this.state.roles.map(({ _id, name }: Role) => {
                                return (
                                    <option key={_id} value={_id} selected={_id === this.state.userData.role._id}>
                                        {name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <input
                            type="submit"
                            value="Edit User"
                            className="btn btn-primary" />
                    </div>

                </form>
            </div>
        );
    }
}

export default UpdateUser;