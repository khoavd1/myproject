import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

interface Roles {
    _id: string;
    name: string;
}
interface User {
    _id: string;
    name: string;
    birthday: Date;
    tel: string;
    email: string;
    role?: Roles;
}
interface ListUserProps {
    userProps: User;
    deleteUser?: any;
}
interface ListUserState {
    users: User[];
}

const UserTable = (props: ListUserProps) => (
    <tr>
        <td>{props.userProps.name}</td>
        <td>{props.userProps.email}</td>
        <td>{props.userProps.birthday}</td>
        <td>{props.userProps.tel}</td>
        <td>{props.userProps.role?.name}</td>
        <td>
            <Link to={"/update/" + props.userProps._id}>Edit</Link> | &nbsp;
            <a href="#" onClick={() => {
                props.deleteUser(props.userProps._id)
            }}>Detele</a>
        </td>
    </tr>
);

class ListUser extends Component<ListUserProps, ListUserState> {
    state = {
        users: []
    };
    componentDidMount() {
        axios.get("/api/users/").then((res) => {
            console.log(res.data)
            this.setState({ users: res.data });
        }).catch((error) => {
            console.log(error);
        });
    }
    deleteUser = (id: any) => {
        axios.delete("http://localhost:3000/" + id).then((res) => {
            alert(res.data);
            this.setState({
                users: this.state.users.filter((el: { _id: any }) => el._id !== id),
            });
        });
    };
    usersList = () => {
        return this.state.users.map((currentuser: User) => {
            return (<UserTable userProps={currentuser}
                deleteUser={this.deleteUser}
                key={currentuser._id} />);
        });
    };


    render() {
        return (
            <div>
                <h3>List User</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Birthday</th>
                            <th>Tel</th>
                            <th>Role</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>{this.usersList()}</tbody>
                </table>
            </div>
        );
    };
}



export default ListUser;