import { Role } from '../role/types';

export const UPDATE_USER_LIST = 'UPDATE_USER_LIST';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const REMOVE_USER = 'REMOVE_USER';

export interface User {
    [key: string]: any;
    _id: string;
    name: string;
    birthday: string;
    tel: string;
    email: string;
    role: Role;
}

export interface UserState {
    users: User[];
}

//Action interfaces

interface UpdateUserListAction {
    type: typeof UPDATE_USER_LIST;
    users: User[];
}

interface AddUserAction {
    type: typeof ADD_USER;
    user: User;
}

interface UpdateUserAction {
    type: typeof UPDATE_USER;
    user: User;
}

interface RemoveUserAction {
    type: typeof REMOVE_USER;
    userId: string;
}

export type UserActionTypes = UpdateUserListAction | AddUserAction | UpdateUserAction | RemoveUserAction;