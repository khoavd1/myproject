import { Controller, Get, Post, Body, Res, UsePipes, ValidationPipe } from '@nestjs/common';
import { UserModel } from 'src/model/user';
import { IsNotEmpty, IsString } from 'class-validator';
import { Response } from 'express';
import { RolesModel } from 'src/model/role';
import { RoleService } from './role.service';


class RoleRequest {
    @IsNotEmpty()
    @IsString()
    name: string
}

@Controller('role')
export class RoleController {
    constructor(private readonly roleService: RoleService) { }

    @Get('/')
    async getAll(): Promise<UserModel[]> {
        return await this.roleService.getRoles();
    }

    @Post('/')
    @UsePipes(new ValidationPipe())
    async addNewRole(@Body() body: RoleRequest, @Res() res: Response): Promise<Response> {
        const input: RolesModel = {
            name: body?.name
        }

        const result = await this.roleService.createRole(input);
        return result ? res.json(result) : res.status(202).json({ message: 'Insert failed' })
    }
}

