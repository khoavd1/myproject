import React, { useState, useCallback, useEffect } from "react";
import "antd/dist/antd.css";
import { Table, Tag, Space, Button, Modal, notification } from "antd";
import {
  EditOutlined,
  CloseOutlined,
  PlusOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../src/store/rootReducer";
import { User } from "../store/user/types";
import { updateRoleList } from "../store/role/actions";
import { updateUserList, removeUser } from "../store/user/action";
import { Role } from "../store/role/types";
import { isNullOrUndefined } from "util";

function UserList() {
  const history = useHistory();
  const dispatch = useDispatch();
  const users = useSelector<RootState, User[]>((state) => state.user.users);
  const [showModal, setModal] = useState(false);

  const fetchUsers = useCallback(async () => {
    return axios.get("/api/users").then(({ data }) => {
      dispatch(updateUserList(data));
      setRefresh(false);
    });
  }, [dispatch]);

  useEffect(() => {
    !users.length && fetchUsers();
  }, [dispatch, users.length, fetchUsers]);

  const [refresh, setRefresh] = useState(false);
  useEffect(() => {
    refresh && fetchUsers();
  }, [refresh, fetchUsers]);

  function handleDelete(id: string) {
    Modal.confirm({
      title: "Bạn chắc chắn xóa",
      okText: "Delete",
      onOk: () => {
        axios
          .delete(`users/${id}`)
          .then(() => {
            dispatch(removeUser(id));
            notification.success({ message: "Xóa thành công!" });
          })
          .catch(({ response }) => {
            const { message } = response.data;
            notification.error({ message });
            dispatch(removeUser(id));
          });
      },
    });
  }
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (_: any, { _id, name }: any) => (
        <Link to={`/users/${_id}`}>{name}</Link>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },

    {
      title: "Role",
      key: "role",
      dataIndex: "role",
      render: (_: any, { role }: any) => (
        <>
          {!isNullOrUndefined(role) ? (
            <Tag
              color={role?.name === "admin" ? "geekblue" : "green"}
              key={role?._id}
            >
              {role?.name.toUpperCase()}
            </Tag>
          ) : (
            <Tag color="red" key={0}>
              NULL
            </Tag>
          )}
        </>
      ),
    },

    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_: any, { _id }: any) => (
        <>
          <Button
            icon={<EditOutlined />}
            onClick={() => history.push(`/users/${_id}/edit`)}
            style={{ marginRight: 5 }}
          ></Button>
          <Button
            icon={<CloseOutlined />}
            onClick={() => handleDelete(_id)}
          ></Button>
        </>
      ),
    },
  ];

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {/* <Button icon={<PlusOutlined />} onClick{() => setModal(true)}>
                    Create user
                </Button> */}
        <Button
          type="primary"
          icon={<ReloadOutlined />}
          loading={refresh}
          onClick={() => setRefresh(true)}
        >
          Refresh
        </Button>
      </div>
      {/* <Modal
                title="Create user"
                visible={showModal}
                onCancel={() => setModal(false)}
                footer={null}
            >
                <UserCreate onSuccess={() => setModal(false)} />
            </Modal> */}

      <Table
        columns={columns}
        dataSource={users}
        rowKey="_id"
        style={{ marginTop: 24 }}
      ></Table>
    </div>
  );
}
export default UserList;

//ReactDOM.render(<Table columns={columns} dataSource={data} />, document.getElementById('container'));
