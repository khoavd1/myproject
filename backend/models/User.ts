import mongoose from 'mongoose';
const Schema = mongoose.Schema;


const UserSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        unique: true,
        require: true
    },
    birthday: {
        type: Date,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    role: {
        type: mongoose.Types.ObjectId,
        ref: "Role",
    },
});

// UserSchema.method('transform', function () {
//     var obj = this
//     //rename fields
//     obj.id = obj._id;
//     obj._id = undefined;
//     return obj;

// });

// UserSchema.virtual('id').get(function () {
//     return this._id.toHexString();
// });

// UserSchema.set('toJSON', {
//     virtuals: true
// })

const User = mongoose.model('User', UserSchema);
export default User;

