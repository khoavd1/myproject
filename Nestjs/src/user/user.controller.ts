import { Controller, Post, Req, Get, Res, Inject } from '@nestjs/common';
import { request, response } from 'express';
import { MONGODB_PROVIDER } from 'src/constants';
import { UserRequest } from 'src/validation/uservalidate'

@Controller('users')
export class UserController {
    constructor(@Inject(MONGODB_PROVIDER) private readonly db: any) {

    }

    @Post()
    async create(@Req() request: Request, @Res() response: Response) {
        this.db.collection('users').insertOne
    }
}