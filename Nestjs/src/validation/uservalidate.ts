import {
    IsOptional, IsMongoId, IsEmail, Matches,
    Validate, IsString, IsNotEmpty,
} from 'class-validator';
import { ObjectId } from 'mongodb';
import { CheckEmailExits } from 'src/validation/custom';

export class UserRequest {
    @Matches(/^[a-zA-Z ]*$/, {
        message: 'Tên không chứa số hoặc ký tự đặc biệt'
    })
    @IsString()
    @IsNotEmpty()
    name: string

    @IsOptional()
    @Validate(CheckEmailExits)
    @IsEmail()
    @IsNotEmpty()
    email: string;

    birthday: string;

    @IsString()
    tel: string;
    @IsMongoId()
    role: ObjectId;
}


