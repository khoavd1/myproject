import React, { Component } from 'react';
import './css/user/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import TopMenu from './Components/TopMenu';
import CreateUser from './Components/CreateUser';
import ListUser from './Components/ListUser';
import UpdateUser from './Components/UpdateUser';
import { BrowserRouter as Router, Route } from "react-router-dom";

interface UserProps {
  link?: any,
  title?: string,
}
interface UserState {

}

const Number: Number[] = [1, 2, 3, 4, 5, 6];

const so3 = Number.map((x) => (<li>So : {x}</li>));//hàm map tương tự foreach

class Class1 extends Component<UserProps, UserState> {
  render() {
    return (
      <div className="col-6">
        <div className="card">
          <img className="card-img-top" src={this.props.link} alt="" />
          <div className="card-body">
            <h4 className="card-title">{this.props.title}</h4>
            <p className="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium modi odit incidunt, veniam sed labore nesciunt repellat. Aliquam minima quo consectetur repellat. Eius quisquam eum nisi assumenda iusto velit enim.</p>
            <div>{so3}</div>
          </div>
        </div>
      </div>
    );
  }
}

function NumberOne(props: any) {
  return (
    <div className="col-6">
      <div className="card">
        <img className="card-img-top" src={props.link} alt="" />
        <div className="card-body">
          <h4 className="card-title">{props.title}</h4>
          <p className="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium modi odit incidunt, veniam sed labore nesciunt repellat. Aliquam minima quo consectetur repellat. Eius quisquam eum nisi assumenda iusto velit enim.</p>

        </div>
      </div>
    </div>
  )
}

function App() {
  return (
    <Router>
      <div className="App container" >
        <div className="row">
          {/* <NumberOne title="ảnh số 1" link="https://cdn.24h.com.vn/upload/2-2020/images/2020-06-01/Linh-Ka-me-ao-nguc-khoe-eo-thon-an-tien-nhat-la-do-ngu-phong-cach-lolita-92948176_2918045351646164_5307732060549414912_n-1590995823-233-width960height932.jpg" />
        <NumberOne title="ảnh số 2" link="https://cdn.24h.com.vn/upload/2-2020/images/2020-06-04/cp-1591239822-519-width660height416.jpg" />
        <Class1 link="https://cdn.24h.com.vn/upload/2-2020/images/2020-06-03/nguoi-ay-la-ai-2-1591160281-209-width660height440.jpg" /> */}
          <TopMenu />
        </div>.
        <Route path="/" exact component={ListUser} />
        <Route path="/create" component={CreateUser} />
        <Route path="/update/:id" component={UpdateUser}  />
      </div>
    </Router>
  );
}

export default App;
