import {
  UserState,
  UserActionTypes,
  UPDATE_USER_LIST,
  ADD_USER,
  REMOVE_USER,
  UPDATE_USER,
  User,
} from "./types";

const initialState: UserState = {
  users: [],
};

export function userReducer(
  state = initialState,
  action: UserActionTypes
): UserState {
  switch (action.type) {
    case UPDATE_USER_LIST:
      return { users: action.users };

    case ADD_USER:
      return {
        users: [...state.users, action.user],
      };

    case UPDATE_USER:
      return {
        users: [
          ...state.users.map<User>((user) => {
            if (user._id === action.user._id) user = action.user;
            return user;
          }),
        ],
      };

    case REMOVE_USER:
      return {
        users: state.users.filter((user) => user._id !== action.userId),
      };

    default:
      return state;
  }
}
