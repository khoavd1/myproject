import { BaseModel } from './base';
import { ObjectId } from 'mongodb';


export interface UserModel extends BaseModel {
    name?: string
    email?: string
    birthday?: string
    tel?: string
    role?: ObjectId
}