import { combineReducers } from 'redux';
import { userReducer } from './user/reducers';
import { roleReducer } from './role/reducers';

export const rootReducer = combineReducers({
    role: roleReducer,
    user: userReducer,
});

export type RootState = ReturnType<typeof rootReducer>;