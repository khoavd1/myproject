import { model, Schema } from "mongoose";


const roleSchema = new Schema({
    name: {
        type: String,
        require: true
    },
});
const Role = model('Role', roleSchema);
export default Role;
