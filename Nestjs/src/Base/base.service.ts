import { BaseModel } from 'src/model/base'
import {
    Collection, FindOneOptions, OptionalId,
    CollectionInsertOneOptions, InsertOneWriteOpResult,
    WithId, FilterQuery, UpdateWriteOpResult, UpdateQuery,
    CollectionAggregationOptions
} from 'mongodb'
import { convertId } from 'src/common/function'


const eqNull = { $eq: null };
export class BaseService<T extends BaseModel>{
    private collection: Collection<T>;
    constructor(collection: Collection<T>) {
        this.collection = collection;
    }

    protected async find(filter: FilterQuery<T>, options?: FindOneOptions): Promise<BaseModel[]> {
        filter.deleteDate = eqNull;
        return this.collection.find(filter, options).map(convertId).toArray();
    }

    protected async findWithAggreGate(filter: FilterQuery<T>, options?: FindOneOptions): Promise<BaseModel[]> {
        filter.deleteDate = eqNull;
        const pipeline = [
            {
                $match: filter
            },
            {
                $lookup: {
                    from: 'roles',
                    localField: 'role',
                    foreignField: '_id',
                    as: 'role'
                }
            },
            {
                $unwind: { path: '$role', preserveNullAndEmptyArrays: true }
            }
        ]
        const result = await this.aggregate(pipeline, options);
        result.map(r => convertId(r));
        return result;
    }

    protected async finOne(filter: FilterQuery<T>, options?: FindOneOptions): Promise<any> {
        filter.deleteDate = eqNull;
        return this.collection.findOne(filter, options);
    }

    protected async findOneWithAggreate(filter: FilterQuery<T>, options?: FindOneOptions): Promise<any> {
        filter.deleteDate = eqNull;
        const pipeline = [
            {
                $match: filter
            },
            {
                $lookup: {
                    from: 'roles',
                    localField: 'role',
                    foreignField: '_id',
                    as: 'role'
                }
            },
            {
                $unwind: { path: '$role', preserveNullAndEmptyArrays: true }
            }
        ]
        const result = await this.aggregate(pipeline, options);
        return convertId(result[0]);
    }

    protected async insertOne(input: OptionalId<T>,
        options?: CollectionInsertOneOptions): Promise<InsertOneWriteOpResult<WithId<T>>> {
        input.createDate = new Date().getTime();
        return this.collection.insertOne(input, options);
    }

    protected async updateOne(filter: FilterQuery<T>, update?: T): Promise<UpdateWriteOpResult> {
        filter.deleteDate = eqNull;
        update.updateDate = new Date().getTime();
        const updateQuery: UpdateQuery<T> = { $set: update };
        return this.collection.updateOne(filter, updateQuery);
    }

    protected async deleteOne(filter: FilterQuery<T>): Promise<UpdateWriteOpResult> {
        filter.deleteDate = eqNull;
        const updateData = { deleteDate: new Date().getTime() } as T;
        const updateQuery: UpdateQuery<T> = { $set: updateData };
        return this.collection.updateOne(filter, updateQuery);
    }


    protected async aggregate(pipeline: any[], options?: CollectionAggregationOptions): Promise<BaseModel[]> {
        return this.collection.aggregate(pipeline, options).toArray();
    }

}

