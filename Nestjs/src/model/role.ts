import { BaseModel } from './base'

export interface RolesModel extends BaseModel {
    name?: string
}