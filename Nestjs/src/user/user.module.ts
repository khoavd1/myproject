import { Module } from "@nestjs/common";
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongoDbModule } from '../mongo-db/mongo-db.module';
import { CheckEmailExits } from 'src/validation/custom';
import { RoleModule } from 'src/role/role.module'


@Module({
    imports: [MongoDbModule.getCollectionService('users'), RoleModule],
    controllers: [UserController],
    providers: [CheckEmailExits, UserService],
    exports: [UserService]
})

export class UserModule { }