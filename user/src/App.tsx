import React from "react";
import "./App.css";
import UserList from "./component/UserList";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

function App() {
  return (
    // <div className="App">
    //   {/* <UserDetail /> */}
    //   <UserList />
    // </div>

    <Provider store={store}>
      <BrowserRouter>
        <Route path="/user" component={UserList} />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
