import { IsEmail, IsDate, Matches, MinLength, MaxLength, MaxDate, IsNotEmpty } from "class-validator";
import "reflect-metadata";
import { Type } from "class-transformer";
import IsEmailAlreadyExist from "../decorators/IsEmailAlreadyExist";

export class CheckValidate {
    
    @Matches(/^[a-zA-Z]*$/, { message: "Tên không được chứa số hoặc ký tự đặt biệt!", })
    @MinLength(2, { message: "Tên phải trên 2 kí tự!", })
    @MaxLength(20, { message: "Tên không được quá 20 kí tự!", })
    @IsNotEmpty({ message: "Không được để trống trường Name!" })
    name: string;

    
    @IsEmail()
    @IsEmailAlreadyExist({ message: "Email đã tồn tại!" })
    @IsNotEmpty({ message: "Không được để trống trường Email!" })
    email: string;

    
    @IsDate()
    @MaxDate(new Date())
    @Type(() => Date)
    @IsNotEmpty({ message: "Không được để trống trường Birthday!" })
    birthday: Date;

    @Matches(/^(?:[0-9] ?){6,14}[0-9]$/, { message: "Sai cú pháp!" })
    tel: string;

    @IsNotEmpty({ message: "Không được để trống trường Role!" })
    role: number;
}
