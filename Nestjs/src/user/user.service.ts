import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../model/user';
import { BaseService } from 'src/Base/base.service';
import { convertId, buildFilterById } from 'src/common/function';
import { Collection } from 'mongodb';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express'


@Injectable()
export class UserService extends BaseService<UserModel>{
    constructor(@Inject(REQUEST) private re: Request, @Inject('users') collection: Collection<UserModel>) {
        super(collection)
    }
    //Get list USERS
    async getAllUser(): Promise<UserModel[]> {
        const result = await this.findWithAggreGate({})
        return result
    }

    //Add User
    async addUser(input: UserModel): Promise<UserModel> {
        const result = await this.insertOne(input)
        return result.insertedCount === 1 ? this.findOneWithAggreate(buildFilterById(convertId(result?.ops[0])?.id)) : null
    }

    //Update User
    async updateUser(id: string, input: UserModel): Promise<UserModel> {
        const update = await this.updateOne(buildFilterById(id), input);
        if (update.modifiedCount === 0) {
            return null;
        }
        const result = await this.findOneWithAggreate(buildFilterById(id));
        return result;
    }

    //Get Detail With Email
    async getUserWithEmail(email: string): Promise<UserModel> {
        const result = await this.findOneWithAggreate({ email: email });
        return result;
    }

    //Delete User
    async deleteUser(id: string): Promise<boolean> {
        const deleteCheck = await this.deleteOne(buildFilterById(id));
        return deleteCheck.modifiedCount > 0;
    }

}