import { RoleState, RoleActionTypes, UPDATE_ROLE_LIST } from './types';

const initialState: RoleState = {
    roles: [],
};

export function roleReducer(
    state = initialState,
    action: RoleActionTypes
): RoleState {
    switch (action.type) {
        case UPDATE_ROLE_LIST:
            return { roles: action.roles };
        default:
            return state;
    }
}