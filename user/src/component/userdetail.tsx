import React, { Component } from 'react';
import logo from '../images/site-logo.png';
import avata from '../images/top-banner-author.jpg';


class UserDetail extends Component {
    render() {
        return (
            <div>
                <nav className="site-navigation">
                    <div className="site-navigation-inner site-container">
                        <img src={logo} alt="site logo" />
                        <div className="main-navigation">
                            <ul className="main-navigation__ul">
                                <li><a href="#">Homepage</a></li>
                                <li><a href="#">Page 1</a></li>
                                <li><a href="#">Page 2</a></li>
                                <li><a href="#">Page 3</a></li>
                                <li><a href="#">Page 4</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <section className="fh5co-top-banner">
                    <div className="top-banner__inner site-container">
                        <div className="top-banner__image">
                            <img src={avata} alt="author image" />
                        </div>
                        <div className="top-banner__text">
                            <div className="top-banner__text-up">
                                <span className="brand-span">Hello! I'm</span>
                                <h2 className="top-banner__h2">Khoa</h2>
                            </div>
                            <div className="top-banner__text-down">
                                <h2 className="top-banner__h2">WWW</h2>
                                <span className="brand-span">Author, Writer, Traveler</span>
                            </div>
                            <p>One Man. One Mission. Can He Go Beyond?One Man. One Mission. Can He Go Beyond?</p>
                            <a href="#" className="brand-button">Read bio &gt; </a>
                        </div>
                    </div>
                </section>

                <footer className="site-footer">
                </footer>
            </div>
        );
    }
}
export default UserDetail