import {
  User,
  UserActionTypes,
  UPDATE_USER_LIST,
  ADD_USER,
  UPDATE_USER,
  REMOVE_USER,
} from "./types";

export function updateUserList(users: User[]): UserActionTypes {
  return {
    type: UPDATE_USER_LIST,
    users: users,
  };
}

export function addUser(user: User): UserActionTypes {
  return {
    type: ADD_USER,
    user: user,
  };
}

export function updateUser(user: User): UserActionTypes {
  return {
    type: UPDATE_USER,
    user: user,
  };
}

export function removeUser(userId: string): UserActionTypes {
  return {
    type: REMOVE_USER,
    userId: userId,
  };
}
