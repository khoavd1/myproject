import {
    registerDecorator,
    ValidationOptions,
    ValidationArguments
} from "class-validator";

import Role from "../../models/Role";

function IsValidRole(ValidationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "isValidRole",
            async: true,
            target: object.constructor,
            propertyName: propertyName,
            options: ValidationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return new Promise((ok) => {
                        Role.findOne({ _id: value }, (err, role) => {
                            !role ? ok(false) : ok(true);
                        });
                    });
                }
            }
        });
    }
}

export default IsValidRole;