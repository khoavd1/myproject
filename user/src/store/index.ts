import { createStore, compose } from "redux";
import { rootReducer } from "./rootReducer";

declare global {
  interface Window {
    _REDUX_DEVTOOLS_EXTENSION__?: typeof compose;
  }
}

const store = createStore(
  rootReducer,
  window._REDUX_DEVTOOLS_EXTENSION__ && window._REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
