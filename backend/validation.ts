import { Request, Response, RequestHandler, NextFunction } from 'express';
import { plainToClass } from "class-transformer";
import { ValidatorOptions, Validator, isMongoId, ValidationError } from 'class-validator';
import HttpStatus from "http-status-codes";


type Constructor<T> = { new(): T };

export function validate<T>(
    type: Constructor<T>,
    validatorOptions: ValidatorOptions = {}): RequestHandler {
    let validator = new Validator();

    return async (req: Request, res: Response, next: NextFunction) => {
        let input = plainToClass(type, req.body);

        let errors = await validator.validate(input, validatorOptions);

        if (errors.length > 0) {
            req.body = input;
            next(errors);

        }
        else {
            next();
        }
    };
}

export function validateMongoId(): RequestHandler {
    return (req: Request, res: Response, next: NextFunction) => {
        if (isMongoId(req.params.id)) {
            next();
        } else {
            res.status(HttpStatus.BAD_REQUEST).json({
                message: "Id không hợp lệ",
            });
        }
    };
}


export function validationError(
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    if (err instanceof Array && err[0] instanceof ValidationError) {
        //format errors
        const responseErrors = err.reduce((errors, currentError) => {
            return {
                ...errors,
                [currentError.property]: {
                    message: Object.values(currentError.constraints)[0],
                },
            };
        }, {});

        res.status(400).json({ errors: responseErrors }).end();
    } else {
        next(err);
    }
}
