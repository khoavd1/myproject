import mongoose from 'mongoose';
import express, { Application } from "express";
import bodyParser from "body-parser";
import userRoutes from "./routes/user"
import roleRoutes from "./routes/role"
import { validationError } from './validation';
import cors from 'cors';


import middlewares from "./middlewares";

const PORT = 3000;


const app: Application = express();
mongoose.connect('mongodb://khoa-training:khoa-training@54.255.220.225/khoa-training', { useUnifiedTopology: true, useNewUrlParser: true }).then(info => {
    // console.log('info', info)
    app.listen(PORT, () => {
        console.log(`Server is running on port: ${PORT}`);
    });
}).catch(err => console.log(err));
app.use(
    cors({
        origin: "*"
    })
)

app.use(bodyParser.json());
app.use('/api/roles', roleRoutes);
app.use('/api/users', userRoutes);

app.use(validationError);
app.use(middlewares.notFound);
app.use(middlewares.errorHandler);
