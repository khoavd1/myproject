import { RoleActionTypes, UPDATE_ROLE_LIST, Role } from './types';

export function updateRoleList(roles: Role[]): RoleActionTypes {
    return {
        type: UPDATE_ROLE_LIST,
        roles: roles,
    };
}