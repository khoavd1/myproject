import { MongoClient } from "mongodb";
import { MONGODB_PROVIDER } from "../constants";
import { Provider } from "@nestjs/common";


export const mongoDbProviders: Provider[] = [
    {
        provide: MONGODB_PROVIDER,
        useFactory: async () => {
            MongoClient.connect('mongodb://localhost:27017',
                { useUnifiedTopology: true },
                (error, client) => {
                    return client.db('nestjs-sample');
                });
        }
    },
];

