import { Request, Response, NextFunction } from "express";
import HttpStatus from "http-status-codes";

const notFound = (req: Request, res: Response, next: NextFunction) => {
    console.log("NotFoundHandler");
    const error = new Error(`Not Found - ${req.originalUrl}`);
    res.status(HttpStatus.NOT_FOUND);
    next(error);
};

const errorHandler = (
    error: Error,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    console.log("ErrorHandler");
    const statusCode =
        res.statusCode ===
            HttpStatus.OK ? HttpStatus.INTERNAL_SERVER_ERROR : res.statusCode;
    res.status(statusCode);
    res.json({
        message: error.message,
        stack: process.env.NODE_ENV === "production" ? "Error" : error.stack,
    });
};

export default {
    notFound,
    errorHandler,
};