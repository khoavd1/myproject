export const UPDATE_ROLE_LIST = 'UPDATE_ROLE_LIST';

export interface Role {
    _id?: string;
    name: string;
}

export interface RoleState {
    roles: Role[];
}

//Action interfaces

interface UpdateRoleListAction {
    type: typeof UPDATE_ROLE_LIST;
    roles: Role[];
}

export type RoleActionTypes = UpdateRoleListAction;